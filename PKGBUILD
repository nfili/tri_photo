# Maintainer: Nicolas Filippozzi <nicolasfilippozzi@gmail.com>

pkgname=tri_photo
pkgver=1.0.1
pkgrel=1
pkgdesc='tri_photo is a program written in rust which allows you to sort photographs and images according to the dates and places of shooting. unstable build'
arch=('x86_64')
url="https://github.com/nfili/tri_photo"
license=('MIT','APACHE')
depends=('cargo' 'gcc-libs' 'gtk4')

source=("git+$url#tag=v$pkgver")
sha512sums=('8eb566691d7cc2d35b863da75a644dc8cc1d087255fc515fbb5415ecd24b18dd54c754e962cc303c870ab04f62d537c252d798d9f930e3e79d602ba223a6c293')
options=(!debug)

prepare() {
	cd "$pkgname"
	export RUSTUP_TOOLCHAIN=stable
    cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
	cd "$pkgname"
	export RUSTUP_TOOLCHAIN=stable
	cargo build --frozen --release --all-features
}

package() {
	cd "$pkgname"

	install -dm755 "${pkgdir}/usr/bin"
	install -Dm755 "target/release/tp" "$pkgdir/usr/bin/tp"

	for res in 512x512 256x256 128x128 80x80 72x72 64x64 48x48 44x44 36x36 32x32 24x24 22x22 20x20 16x16 8x8; do
		install -dm755 "${pkgdir}/usr/share/icons/hicolor/${res}/apps"
		install -Dm644 "icons/${res}/tp.png" "${pkgdir}/usr/share/icons/hicolor/${res}/apps/tp.png"
	done

	for lang in `ls locale`; do
		install -dm755 "/usr/share/locale/${lang}/LC_MESSAGES/"
		install -Dm644 "locale/${lang}/LC_MESSAGES/tp.mo" "${pkgdir}/usr/share/locale/${lang}/LC_MESSAGES/tp.mo"
	done

	install -dm755 "${pkgdir}/usr/share/applications"
	install -Dm644 'tp.desktop' "${pkgdir}/usr/share/applications/tp.desktop"

	install -Dm644 "README.md" "$pkgdir/usr/share/doc/${pkgname}/README.md"
	install -Dm644 "COPYRIGHT.md" "$pkgdir/usr/share/doc/${pkgname}/COPYRIGHT"
	install -Dm644 "CHANGELOG.md" "$pkgdir/usr/share/doc/${pkgname}/CHANGELOG"
  	install -Dm644 "LICENSE-MIT" "$pkgdir/usr/share/licenses/${pkgname}/LICENSE-MIT"
  	install -Dm644 "LICENSE-APACHE" "$pkgdir/usr/share/licenses/${pkgname}/LICENSE-APACHE"
  	


}
